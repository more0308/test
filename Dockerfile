FROM node:lts-alpine

WORKDIR /app

COPY ./ /app

CMD [ "npm", "run", "start:dev" ]
