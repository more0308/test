import { Controller, Get, Query } from '@nestjs/common';
import { TaskService } from './task.service';
import { GetAllTasksDto } from './dto/get-all-tasks.dto';

@Controller('task')
export class TaskController {
  constructor(private service: TaskService) {}

  @Get()
  getAll(@Query() query: GetAllTasksDto) {
    return this.service.getAll(query);
  }
}
