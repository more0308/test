import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Task } from './task.entity';
import { Job } from '../job/job.entity';
import { GetAllTasksDto } from './dto/get-all-tasks.dto';

@Injectable()
export class TaskService {
  constructor(
    @InjectRepository(Task)
    private readonly repository: Repository<Task>,
  ) {}

  async get(id: Task['id']) {
    return await this.repository.findOne({ where: { id } });
  }

  async getAll(filter: GetAllTasksDto) {
    const percentage = filter.percentage || 0;
    return await this.repository
      .createQueryBuilder('t')
      .select([
        't.*',
        'COALESCE(SUM(j.cost), 0) as job_sum',
        '(COALESCE(SUM(j.cost), 0)::numeric / t.cost * 100) as percentage_used',
      ])
      .leftJoin(
        (subQuery) =>
          subQuery
            .select('"taskId"', 'taskId')
            .addSelect('SUM(cost) as cost')
            .from(Job, 'j')
            .groupBy('"taskId"'),
        'j',
        't.id = j."taskId"',
      )
      .groupBy('t.id')
      .having(
        '(COALESCE(SUM(j.cost), 0)::numeric / t.cost * 100) >= :percentage',
        { percentage },
      )
      .getRawMany();
  }
}
