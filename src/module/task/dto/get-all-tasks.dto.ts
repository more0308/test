import { IsNumber, IsOptional, Max, Min } from 'class-validator';
import { Transform } from 'class-transformer';

export class GetAllTasksDto {
  @IsNumber()
  @IsOptional()
  @Transform(({ value }) => parseInt(value, 10))
  @Min(0, { message: 'Value must be at least 0' })
  @Max(100, { message: 'Value must be at most 100' })
  readonly percentage!: number;
}
