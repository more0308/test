import { Injectable } from '@nestjs/common';
import { CreateJobDto } from './dto/create-job.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Job } from './job.entity';
import { Repository } from 'typeorm';
import { Task } from '../task/task.entity';

@Injectable()
export class JobService {
  constructor(
    @InjectRepository(Job)
    private readonly repository: Repository<Job>,
  ) {}
  async create(data: CreateJobDto, moneySpent: number) {
    await this.repository.save({
      ...data,
      cost: moneySpent,
    });
  }

  async getAmountSpent(task: Task): Promise<number> {
    const data = await this.repository
      .createQueryBuilder('jobs')
      .select('SUM(jobs.cost)', 'sum')
      .where('jobs.taskId = :taskId', { taskId: task })
      .getRawOne();

    return Number(data.sum);
  }

  calculateRoundedHoursDifference(startTime: Date, endTime: Date): number {
    const timeDifferenceInMilliseconds =
      endTime.getTime() - startTime.getTime();
    const minutesDifference = timeDifferenceInMilliseconds / (1000 * 60);

    return (
      Math.floor(minutesDifference / 60) + (minutesDifference % 60 > 15 ? 1 : 0)
    );
  }
}
