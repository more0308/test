import { Task } from '../../task/task.entity';
import { User } from '../../user/user.entity';
import { IsDate, IsDefined, IsNotEmpty } from 'class-validator';
import { Transform } from 'class-transformer';

export class CreateJobDto {
  @IsDefined()
  @IsNotEmpty()
  readonly task: Task;

  @IsDefined()
  @IsNotEmpty()
  readonly user: User;

  @Transform(({ value }) => new Date(value))
  @IsDate()
  @IsDefined()
  @IsNotEmpty()
  readonly startTime: Date;

  @Transform(({ value }) => new Date(value))
  @IsDate()
  @IsDefined()
  @IsNotEmpty()
  readonly endTime: Date;
}
