import { BadRequestException, Body, Controller, Post } from '@nestjs/common';
import { CreateJobDto } from './dto/create-job.dto';
import { JobService } from './job.service';
import { TaskService } from '../task/task.service';
import { UserService } from '../user/user.service';

@Controller('job')
export class JobController {
  constructor(
    private service: JobService,
    private taskService: TaskService,
    private userService: UserService,
  ) {}

  @Post()
  async create(@Body() data: CreateJobDto) {
    if (data.startTime >= data.endTime) {
      throw new BadRequestException('Incorrect date');
    }

    const amountSpent = await this.service.getAmountSpent(data.task);
    const task = await this.taskService.get(+data.task);
    const user = await this.userService.get(+data.user);

    const spendTime = this.service.calculateRoundedHoursDifference(
      data.startTime,
      data.endTime,
    );

    const totalSpendMoney = amountSpent + user.rate * spendTime;
    if (totalSpendMoney > task.cost) {
      throw new BadRequestException('Excess amount of funds');
    }

    await this.service.create(data, user.rate * spendTime);

    return {
      result: ((totalSpendMoney / task.cost) * 100).toFixed(2),
    };
  }
}
