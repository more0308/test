import { Module } from '@nestjs/common';
import { JobModule } from './module/job/job.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { TaskModule } from './module/task/task.module';
import { UserModule } from './module/user/user.module';
import databaseConfig from './config/database.config';

@Module({
  imports: [
    ConfigModule.forRoot({ envFilePath: '.env' }),
    TypeOrmModule.forRoot(databaseConfig()),
    JobModule,
    TaskModule,
    UserModule,
  ],
})
export class AppModule {}
